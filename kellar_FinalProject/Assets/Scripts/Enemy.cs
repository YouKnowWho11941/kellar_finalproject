﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour {

	public int health;

	public GameObject treasure;
    public GameObject gold;
	GameObject player;//tried to get the location of the enemy and only have monsters appear if player is within 20 units

	public float minDist = 5f;
	PlayerScript playerScript;
	public float timeBtwAttack;
	public float startTimeBtwAttack;
	private float timer = 2f;
	public float enemydamage;
	private Animator animator;

	public Image damageImage;
	public float flashSpeed = 5f;
    public Text GoldText;


    // Use this for initialization
    void Start () {

		player = GameObject.FindGameObjectWithTag ("Player");
		playerScript = player.GetComponent <PlayerScript> ();
		animator = GetComponent<Animator> ();
		timer = startTimeBtwAttack;

    }
	
	// Update is called once per frame
	void Update () {
		//Vector3 between = player.transform.position - transform.position;
		float distance = Vector3.Distance(player.transform.position, transform.position);
        //print("distance: "+ distance );
        if (distance < minDist)
        {
            if (timer <= 0)
            {
                //print("Enemy senses player");
				animator.SetTrigger ("enemyAttack");
                playerScript.TakeDamage(enemydamage);
                timer = timeBtwAttack;


            }
            else
            {
                timer -= Time.deltaTime;
            }
        }
        // (distance >= 50f)
			

		if (health <=0)
		{
			Destroy(gameObject);
            if (playerScript.totaltime <= 30f)
            {
                Instantiate(treasure, transform.position, Quaternion.identity);
                print("time");
            }
            else
            {
                Instantiate(gold, transform.position, Quaternion.identity);
                print("gold");
            }

		}
	}

	public void TakeDamage(int damage)
	{
		health -= damage;

	}


}
