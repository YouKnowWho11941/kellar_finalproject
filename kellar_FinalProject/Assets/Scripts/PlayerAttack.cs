﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

	private Animator animator;

	public float timeBetweenAttack;
	public float startTimeBetweenAttack;

	public Transform attackPos;
	public LayerMask whatIsEnemies;
	public float attackRange;
	public int damage;

	// Use this for initialization
	void Start () {
		animator= GetComponent<Animator>();
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*if (timeBetweenAttack <= 0)
		{
			if(Input.GetKey(KeyCode.Space))
			{
				Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
				for (int i =0; i< enemiesToDamage.Length; i++)
				{
					enemiesToDamage[i].GetComponent<Enemy>().TakeDamage(damage);//health -= damage;
					//print("ATTACK");
					animator.SetTrigger("playerChop");
				}
			}
			timeBetweenAttack = startTimeBetweenAttack;
		}
		else
		{
			timeBetweenAttack -= Time.deltaTime;
		}*/

        if (Input.GetKey(KeyCode.Space))
        {
            
            if (timeBetweenAttack <= 2)
            {
				animator.SetTrigger("playerChop");
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<Enemy>().TakeDamage(damage);//health -= damage;
                }
                timeBetweenAttack = startTimeBetweenAttack;

            }
            else
            {
                timeBetweenAttack -= Time.deltaTime;
            }
        }

    }

	
}
