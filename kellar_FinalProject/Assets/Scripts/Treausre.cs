﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Treausre : MonoBehaviour {

	GameObject Player;
	PlayerScript playerScript;

    GameObject goldTextObject;
    Text goldText;

    // Use this for initialization
    void Awake () 
	{

		Player = GameObject.FindGameObjectWithTag ("Player");
		playerScript = Player.GetComponent <PlayerScript> ();

        goldTextObject = GameObject.FindGameObjectWithTag("GoldText");
        goldText = goldTextObject.GetComponent<Text>();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerExit2D(Collider2D other)
	{
    	if (other.gameObject.CompareTag("Player"))
    	{

			playerScript.treasurecount += Random.Range(15, 25);
            goldText.text = playerScript.treasurecount.ToString();
            print("treasure given"+ playerScript.treasurecount);

            Destroy(gameObject);

    	}
	}
}
