﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Exit : MonoBehaviour {

    GameObject LossImage;
    Color black = new Color(0f, 0f, 0f, 1f);
    Image lossImage;
    GameObject WinText;
    Color white = new Color(255f, 255f, 255f, 255f);
    Text winText;

    GameObject Player;
	PlayerScript playerScript;
    AudioSource backgroundMusic;
	// Use this for initialization
	void Start () {

        LossImage = GameObject.FindGameObjectWithTag("LossImage");
        lossImage = LossImage.GetComponent<Image>();

        WinText = GameObject.FindGameObjectWithTag("WinText");
        winText = WinText.GetComponent<Text>();	

        Player = GameObject.FindGameObjectWithTag ("Player");
		playerScript = Player.GetComponent <PlayerScript> ();
        backgroundMusic = Player.GetComponent <AudioSource>();
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Time.timeScale = 0f;
            lossImage.color = black;
            winText.text = "You made it out alive.\nYou COULD have been better though,\nyou only made it out with " + playerScript.treasurecount + " pieces of treasure.";
            winText.color = white;
            backgroundMusic.volume = .3f;

        }
    }
}
