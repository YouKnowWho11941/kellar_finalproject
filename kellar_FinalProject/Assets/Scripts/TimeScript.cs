﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimeScript : MonoBehaviour
{

//    GameObject airTextObject;
//    Text airText;

    GameObject Player;
    PlayerScript playerScript;

    // Use this for initialization
    void Awake()
    {

        Player = GameObject.FindGameObjectWithTag("Player");
        playerScript = Player.GetComponent<PlayerScript>();

 //       airTextObject = GameObject.FindGameObjectWithTag("AirText");
 //       airText = airTextObject.GetComponent<Text>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {

            playerScript.totaltime += 20f;
            playerScript.UpdateAir(playerScript.totaltime);
            print("more time");
            Destroy(gameObject);
        }
    }
    
}
