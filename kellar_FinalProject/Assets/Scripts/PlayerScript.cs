﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerScript : MonoBehaviour {

    //public Slider airSliderR;
    //public Slider airSliderL;
    //public Slider healthSlider;

    Rigidbody2D rb;
    public float speed = 1f;
    public float health = 15f;

    //Player player;
    public float totaltime = 10f;
    public float treasurecount = 0f;


    Animator playeranim;

    GameObject healthSliderObject;
    Slider healthSlider;
    GameObject healthTextObject;
    Text healthText;

    GameObject airTextObject;
    Text airText;
    GameObject airSliderObject;
    Slider airSlider;
   

    GameObject DamageImage;
    Image damageImage;
    public float flashSpeed = 5f;                              
    public Color flashColour = new Color(0f, 0f, 0f, 1f);  
  
    GameObject LossImage;
    Color black = new Color(0f, 0f, 0f, 1f);
    Image lossImage;
    GameObject LossText;
    Color white = new Color(255f, 255f, 255f, 255f);
    Text lossText;





    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody2D>();

        airTextObject = GameObject.FindGameObjectWithTag("AirText");
        airText = airTextObject.GetComponent<Text>();
        airSliderObject = GameObject.FindGameObjectWithTag("airSlider");
        airSlider = airSliderObject.GetComponent<Slider>();

        playeranim = GetComponent<Animator>();

        healthSliderObject = GameObject.FindGameObjectWithTag("healthSlider");
        healthSlider = healthSliderObject.GetComponent<Slider>();
        healthTextObject = GameObject.FindGameObjectWithTag("HealthText");
        healthText = healthTextObject.GetComponent<Text>();

        DamageImage = GameObject.FindGameObjectWithTag("DamageImage");
        damageImage = DamageImage.GetComponent<Image>();

        LossImage = GameObject.FindGameObjectWithTag("LossImage");
        lossImage = LossImage.GetComponent<Image>();

        LossText = GameObject.FindGameObjectWithTag("LossText");
        lossText = LossText.GetComponent<Text>();

    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        damageImage.color= flashColour;
        damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        print("health: " + health);
        healthSlider.value = health;
        playeranim.SetTrigger("playerHit");
        healthText.text = health.ToString();
        //WaitForSeconds(2);
        //damageImage.color = offColour;

    }

    public void UpdateAir(float newtime)
    {
        int airInt;
        airInt = (int)newtime;
        airText.text = airInt.ToString();
        //print(totaltime);
    }


    private void Update()
    {

        totaltime -= Time.deltaTime;
        UpdateAir(totaltime);
        airSlider.value = totaltime;
        //print(totaltime);
        //AirText.text = totaltime;
        if (totaltime <= 0f)
        {
            print("death by time");
            Death();
        }

        if (health <= 0f)
        Death();

        //print(totaltime);
    }
    void FixedUpdate () {


        var move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        //rb.velocity += move * speed * Time.deltaTime;
        rb.MovePosition(transform.position + move * speed * Time.deltaTime);

    }

    public void Death()
    {
        Time.timeScale = 0f;
        lossImage.color= black;
        lossText.color= white;
        
    }
}
